import React from 'react';
import MainNavbar from './components/MainNavbar';

import { BrowserRouter as Router, Route } from "react-router-dom";

import Homepage from './pages/Homepage';

import ManageQuestions from './pages/ManageQuestions';
import AddQuestion from './pages/AddQuestion';
import EditQuestion from './pages/EditQuestion';

import ManageQuestionnaires from './pages/ManageQuestionnaires';
import AddQuestionnaire from './pages/AddQuestionnaire';
import EditQuestionnaire from './pages/EditQuestionnaire';

import ExamPage from './pages/Exam/';


function App() {
  return (
    <div>

      <Router>
      <MainNavbar />
        <Route path="/" />
        <Route path="/manage-questions" exact component={ManageQuestions} />
        <Route path="/manage-questions/add-question" exact component={AddQuestion} />
        <Route path="/manage-questions/question/:id" exact component={EditQuestion} />
        <Route path="/manage-questionnaires" exact component={ManageQuestionnaires} />
        <Route path="/manage-questionnaires/add-questionnaire" exact component={AddQuestionnaire} />
        <Route path="/manage-questionnaires/questionnaire/:id" exact component={EditQuestionnaire} />
        <Route path="/exam/:id" exact component={ExamPage} />




      </Router>
    </div>
  );
}

export default App;

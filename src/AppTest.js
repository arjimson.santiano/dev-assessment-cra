
import React, { useState, useEffect } from 'react';



function App() {

    const [timeAllot, setTimeAllot] = useState(3);

    const [isRunning, setIsRunning] = useState(false);

    const [questionCollection, setQuestionCollection] = useState([
        "Whats your name?", "Whats your age?", "Whats your address?"
    ])

    const [counter, setCounter] = useState(0)

    const [question, setQuestion] = useState("");

    useEffect(() => {
        const interval = setInterval(() => tick(), 1000);
        return () => {
            clearInterval(interval);
        }
    });

    const tick = () => {
        if(isRunning) {
            if(timeAllot > 0) {
                setTimeAllot(timeAllot - 1)
            } else {
                if(questionCollection.length > 0) {
                    nextQuestion();
                    setTimeAllot(2);
                } else {
                    setIsRunning(false);
                }
            } 
        } 
    }

    const getTime = () => {
        setIsRunning(!isRunning);
    }

    const resetTime = () => {
        setTimeAllot(100);
    }

    const nextQuestion = () => {
        let newCounter = counter + 1;
        setCounter(newCounter);
        setQuestion(questionCollection[counter]);
    }

    console.log(timeAllot);

    return (
        <div>
            <p>{timeAllot}</p>
            <Question question={question} />
            {/* <p>{timeAllot}</p> */}
            <button onClick={() => nextQuestion()}>next</button>

            <button onClick={() => getTime()}>getTime</button>
            <button onClick={() => resetTime()}>reset</button>

        </div>
    )
}


// const StopWatch  = (props) => {

//     const { isRunning } = props;

//     const [counter, setCounter] = useState(props.timeX);
  
//     useEffect(() => {

//     const interval = setInterval(() => tick(), 1000);
  
//       return () => {
//         clearInterval(interval);
//       };
//     });

//     const tick = () => {
//         if(isRunning) {
//             setCounter(counter - 1)
//         } 
//     }

//     return <h1>{counter}</h1>;

//   };

const Question = (props) => {
    return <h1>{props.question}</h1>
}

export default App;
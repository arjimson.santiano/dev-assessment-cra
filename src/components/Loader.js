import React from 'react';
import { Spinner } from 'reactstrap';


function Loader(props) {


    return (
        <div className="d-flex h-100 justify-content-center align-items-center" style={style.loader}>
            <Spinner color="secondary" />
        </div>
    )
}

const style = {
    loader : {
        border: "1px solid black",
        position: "absolute",
        top: "0",
        width: "100vw",
        background: "rgba(0,0,0,.1)"
    }
}
export default Loader;
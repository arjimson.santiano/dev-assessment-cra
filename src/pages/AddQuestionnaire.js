import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Col, Row, Container, Input, InputGroup, InputGroupAddon, InputGroupText, Button } from 'reactstrap';
import useInputState from "../hooks/usetInputState";
import axios from 'axios';

import AllQuestions from './allQuestions';
import QuestionsSelected from './questionsSelected';
import Loader from '../components/Loader';

function AddQuestionnaire(props) {

    const { value, handleChange, reset } = useInputState();

    const [data, setData] = useState({
        questions: [],
        questionsSelected: []
    });

    const [isLoading, setisLoading] = useState(true);

    useEffect(() => {
        let fetchQuestions = setTimeout(() => getQuestions(), 250);
        return () => {
            clearTimeout(fetchQuestions);
        }
        
    }, [])

    const onSubmit = async (e) => {
        e.preventDefault();
        let allQuestionsId = await data.questionsSelected.map(q => { return q._id});
        let newQuestionnaire = await { ...value, questions: allQuestionsId};

        axios.post("http://localhost:5000/api/questionnaires", newQuestionnaire).then(
            props.history.push('/manage-questionnaires')
        )
            .catch(err => {
                console.log(err);
            });;
    }


    const getQuestions = () => {
        axios
            .get("http://localhost:5000/api/questions")
            .then(res => {
                setData({ ...data, questions: res.data });
                setisLoading(false)
            })
            .catch(err => {
                console.log(err);
            });

    }

    const onSelect = (id) => {
        console.log(id);
        let isExist = questionExist(id);
        console.log(isExist);

        let newQuestions = data.questions;
        newQuestions.map((n, index) => n._id === id ? newQuestions[index].isSelected = true : n);
        // newQuestions[idx].isSelected = true;

        if (isExist) {

        } else {
            setData({
                ...data,
                questions: newQuestions,
                questionsSelected: [...data.questionsSelected, data.questions.find(q => q._id === id)]
            })
        }

    }

    console.log(data);

    const onUnSelect = (id) => {
        // console.log(id);
        // let newQuestions = data.questions;
        // newQuestions[idx].isSelected = true;
        let newQuestions = data.questions;
        newQuestions.map((n, index) => n._id === id ? newQuestions[index].isSelected = false : n);

        setData({
            ...data,
            questions: newQuestions,
            questionsSelected : data.questionsSelected.filter( q=> q._id !== id)
        })
    }

    const questionExist = (id) => {
        let isExist = data.questionsSelected.find(q => q._id === id);
        if (isExist) {
            return true;
        }
        return false;

    }

    const questionsLeft = data.questions.map((question) => {
        return <AllQuestions key={question._id} {...question} onSelect={onSelect} />
    })

    const questionsRight = data.questionsSelected.map(questionSelected => {
        return <QuestionsSelected key={questionSelected._id} {...questionSelected} onUnSelect={onUnSelect} />
    })


    return ( isLoading ? <Loader /> : <Container>
        <Link to="/manage-questionnaires" className="btn btn-dark mt-2 cstm-btn">Back</Link>
        <h3>Add Questionnaire</h3>
        <div className="mt-2">
            <form onSubmit={onSubmit}>
                <InputGroup size="sm" className="mb-2">
                    <InputGroupAddon addonType="prepend">Title:</InputGroupAddon>
                    <Input type="text" placeholder="Enter title here" name="title" onChange={handleChange} />
                </InputGroup>
                <InputGroup size="sm" className="mb-2">
                    <InputGroupAddon addonType="prepend">Description:</InputGroupAddon>
                    <Input type="text" placeholder="Enter description here" name="description" onChange={handleChange} />
                </InputGroup>

                <Row style={{ height: "500px" }}>

                    <Col md="6">
                       
                        {/* <div style={{ height: "30px"}}> */}
                        <div>
                            Pick Questions here
                            {/* <Input type="text" className="w-50 float-right" size="sm" placeholder="search your question here" /> */}
                        </div>
                        <div className="mt-2" style={style.questionsLeft}>
                            {questionsLeft}
                        </div>
                    </Col>
                    <Col md="6">
                        Selected Questions
                        <div className="mt-2" style={style.questionsRight}>
                            {questionsRight}
                        </div>

                    </Col>
                </Row>

                <div className="mt-2">
                    <Button className="btn btn-dark mr-2 cstm-btn">Submit</Button>
                    <Link className="btn btn-dark cstm-btn" to="/manage-questionnaires">Cancel</Link>
                </div>

            </form>
        </div>
    </Container>)
       
    
}

const style = {
    table: {
        border: "1px solid #ced4da",
        width: "100%",
        borderRadius: ".25rem",
        padding: ".5rem .35rem",
        overflowY: "auto"
    },
    questionsLeft: {
        border: "1px solid #ced4da",
        borderRadius: ".25rem",
        padding: "5px",
        height: "470px",
        overflowY: "auto"
    },
    questionsRight: {
        border: "1px solid #ced4da",
        borderRadius: ".25rem",
        padding: "5px",
        height: "470px",
        overflowY: "auto"
    },
    questionSelected: {
        cursor: "no-drop"
    },
    isSelected : {
        background: "#ccc"
    }
}

export default AddQuestionnaire;
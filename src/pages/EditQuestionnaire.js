import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Col, Row, Container, Input, InputGroup, InputGroupAddon, InputGroupText, Button } from 'reactstrap';
import useInputState from "../hooks/usetInputState";
import axios from 'axios';

import AllQuestions from './allQuestions';
import QuestionsSelected from './questionsSelected';
import Loader from '../components/Loader';

function EditQuestionnaire(props) {

    const { value, handleChange, reset, setValue } = useInputState({
        _id: "",
        title : "",
        description : "",
        questions: []
    });
    const [questions, setQuestions] = useState([]);

    const [isLoading, setisLoading] = useState(true);

    const [data, setData] = useState({
        questions: [],
        questionsSelected: []
    });

    useEffect(() => {
        let fetchQuestions = setTimeout(() => getQuestions(), 250); 
        return () => {
            clearTimeout(fetchQuestions);
        }
    }, [])

    useEffect(() => {
        getQuestionnaires()
    }, [])

    const onSubmit = async (e) => {
        e.preventDefault();
        let updateQuestionnaire = {
            _id : value._id
            ,title : value.title
            ,description: value.description 
            ,questions : value.questions.map(q=> {return(q._id)})
        }

        axios.patch(`http://localhost:5000/api/questionnaires/update/${value._id}`, value).then(res => {
            props.history.push("/manage-questionnaires")
        })

        console.log(updateQuestionnaire)
    }

    const getQuestions = () => {
        axios
            .get("http://localhost:5000/api/questions")
            .then(res => {
                let id = props.match.params.id;
                axios.get(`http://localhost:5000/api/questionnaires/${id}`).then(res1 =>{
                    let ques = res.data;
                    let allQuestionsSelectedId = res1.data.questions.map(q => { return q._id});
                    ques.map((q, index) => allQuestionsSelectedId.includes(q._id) ?  ques[index].isSelected = true  : "");
                    setQuestions(ques);
                    setisLoading(false);
                })
            })
            .catch(err => {
                console.log(err);
            });
    }

    const getQuestionnaires = async () => {
        let id = await props.match.params.id;
        axios.get(`http://localhost:5000/api/questionnaires/${id}`)
            .then(res => {
                setValue(res.data)
            })
    }

    const checkSelectedQuestions = () => {
        let newQuestions = questions;
        console.log(value.questions, "ar")
    }


    const onSelect = (id) => {
        let isExist = questionExist(id);

        let newQuestions = questions;
        newQuestions.map((n, index) => n._id === id ? newQuestions[index].isSelected = true : n);

        if (!isExist) {
            setQuestions(newQuestions);
            setValue({...value, questions : [...value.questions, questions.find(q => q._id === id)]})
        }
        return false
    }

    const onUnSelect = (id) => {

        let newQuestions = questions;
        newQuestions.map((n, index) => n._id === id ? newQuestions[index].isSelected = false : n);

        setQuestions(newQuestions);

        setValue({...value, questions : value.questions.filter( q => q._id !== id)})
    }

    const questionExist = (id) => {
        let isExist = value.questions.find(q => q._id === id);
        if (isExist) {
            return true;
        }
        return false;

    }

    const questionsLeft =  questions.map(question => {
        return <AllQuestions key={question._id} {...question} onSelect={onSelect} />
    })

    const questionsRight = value.questions.map(question => {
        return <QuestionsSelected key={question._id} {...question} onUnSelect={onUnSelect} />
    })


    return isLoading ? <div style={{ marginTop: "200px"}}><Loader /></div> : <Container>
        <Link to="/manage-questionnaires" className="btn btn-dark mt-2 cstm-btn" style={style.buttonStyle}>Back</Link>
        <h3>Add Questionnaire</h3>
        <div className="mt-2">
            <form onSubmit={onSubmit}>
                <InputGroup size="sm" className="mb-2">
                    <InputGroupAddon addonType="prepend">Title:</InputGroupAddon>
                    <Input type="text" placeholder="Enter title here" name="title" onChange={handleChange} defaultValue={value.title} />
                </InputGroup>
                <InputGroup size="sm" className="mb-2">
                    <InputGroupAddon addonType="prepend">Description:</InputGroupAddon>
                    <Input type="text" placeholder="Enter description here" name="description" onChange={handleChange} defaultValue={value.description} />
                </InputGroup>

                <Row style={{ height: "500px" }}>

                    <Col md="6">
                       
                        {/* <div style={{ height: "30px"}}> */}
                        <div>
                            Pick Questions here
                            {/* <Input type="text" className="w-50 float-right" size="sm" placeholder="search your question here" /> */}
                        </div>
                        <div className="mt-2" style={style.questionsLeft}>
                            {questionsLeft}
                        </div>
                    </Col>
                    <Col md="6">
                        Selected Questions
                        <div className="mt-2" style={style.questionsRight}>
                            {questionsRight}
                        </div>

                    </Col>
                </Row>

                <div className="mt-2">
                    <Button className="btn btn-dark mr-2 cstm-btn">Update</Button>
                    <Link className="btn btn-dark cstm-btn"  to="/manage-questionnaires">Cancel</Link>
                </div>

            </form>
        </div>
    </Container>
}

const style = {
    table: {
        border: "1px solid #ced4da",
        width: "100%",
        borderRadius: ".25rem",
        padding: ".5rem .35rem",
        overflowY: "auto"
    },
    questionsLeft: {
        border: "1px solid #ced4da",
        borderRadius: ".25rem",
        padding: "5px",
        height: "470px",
        overflowY: "auto"
    },
    questionsRight: {
        border: "1px solid #ced4da",
        borderRadius: ".25rem",
        padding: "5px",
        height: "470px",
        overflowY: "auto"
    },
    questionSelected: {
        cursor: "no-drop"
    }
}

export default EditQuestionnaire;
import React from 'react';
// reactstrap
import { Input } from 'reactstrap';

function ExamAnswer(props) {

    const {handleChange, value} = props;

    return (
        <div>
            <Input 
            type="text" 
            placeholder="Enter your answer here" 
            value={value} 
            onChange={handleChange} />
        </div>
    )
}

export default ExamAnswer;
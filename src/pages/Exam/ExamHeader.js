import React from 'react';

function ExamHeader(props) {
    const { title, description  } = props;
    return (
        <div>


            <p>Title: {title}</p>
            <p>Description: {description}</p>
        </div>
    )
}

export default ExamHeader;
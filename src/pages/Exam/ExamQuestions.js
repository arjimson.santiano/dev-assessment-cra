import React from 'react';

function ExamQuestion(props) {
    const { questionsCount, questionsTotal } = props;
    return (
        <div>
            <p><p>Question {questionsCount} out of {questionsTotal}</p></p>
            <h3>{props.question}</h3>
        </div>
    )
}

export default ExamQuestion;
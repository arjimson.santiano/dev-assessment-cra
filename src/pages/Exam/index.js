import React, { Component, useState, useEffect } from 'react';
// axios
import axios from 'axios';
// reacstrap
import { Container, Button, Input } from 'reactstrap';
// components
import ExamHeader from './ExamHeader';
import ExamQuestion from './ExamQuestions';
import ExamAnswer from './ExamAnswer';
import TimeAllotment from './TimeAllotment';
// loader
import Loader from '../../components/Loader';

const questionnaireIV = {
    description: "",
    questions: [],
    title: ""
}

const quizIV = {
    counter: 0,
    questionId: 1,
    question: '',
    answerOptions: [],
    answer: '',
    timeAllotment: 0,
    allAnswers: [],
    answersCount: {},
    result: ''
}

function Exam(props) {

    const [questionnaire, setQuestionnaire] = useState(questionnaireIV);
    const [quiz, setQuiz] = useState(quizIV);
    const [result, setResult] = useState(false);
    const [isLoading, setisLoading] = useState(true);
    const [isStart, setIsStart] = useState(false);


    console.log(questionnaire, "questionnaire")


    useEffect( () => {
        let fetchQuestionnaire = setTimeout(() => getQuestionnaire(), 250);
    }, [])

    const getQuestionnaire = () => {
        let id = props.match.params.id;
        axios.get(`http://localhost:5000/api/questionnaires/${id}`)
            .then(res => {
                setQuestionnaire(res.data);
                setQuiz({...quiz, question: res.data.questions[0].question, timeAllotment : res.data.questions[0].timeAllotment})
                setisLoading(!isLoading);
            });
    }; 

    const handleChange = (e) => {

        setQuiz({...quiz, answer : e.target.value })
    }

    const setNextQuestion = () => {
        const counter = quiz.counter + 1;
        const questionId = quiz.questionId + 1;
        const question = questionnaire.questions[counter].question;
        const timeAllotment =questionnaire.questions[counter].timeAllotment;
        setQuiz({
          ...quiz,
          counter: counter,
          questionId: questionId,
          question: question,
          timeAllotment: timeAllotment,
          answer: ""
        });
    }

    const setApplicantAnswer = (answer) => {
        let newAnswers = quiz.allAnswers.push(answer);
        setQuiz((state) => ({
            ...quiz,
          allAnswers : newAnswers,
          answersCount: {
            ...quiz.answersCount,
            [answer]: (quiz.answersCount[answer] || 0) + 1
          }
        }));
    }
    
    const handleAnswerInput = () => {
        let questionsCount = questionnaire.questions.length;
        if(quiz.questionId < questionsCount) {
            setApplicantAnswer(quiz.answer);
            setTimeout(() =>  setNextQuestion(), 300);
        } else {
            setResult(true)
        }
    }

    const goNextQuestion = () => {
        setNextQuestion();
    }

    console.log(questionnaire)


    const renderQuiz = () => {
        return (
            <Container>
                <div className="mt-5"> 
                    <ExamHeader 
                    title={questionnaire.title} 
                    description={questionnaire.description}
                
                    />
                </div>
                <div style={myStyle.examBody}>
                    <ExamQuestion 
                    questionsTotal={questionnaire.questions.length} 
                    questionsCount={quiz.questionId}   
                    question={quiz.question}/>

                    <ExamAnswer 
                    value={quiz.answer} 
                    handleChange={handleChange} />

                    <Button className="mt-2" onClick={handleAnswerInput}>Next</Button>
                </div>
                <div className="mt-5">
                    <TimeAllotment timeAllotment={quiz.timeAllotment} goNextQuestion={goNextQuestion}/>
                </div>
            </Container>
        )
    }

    const renderResult = () => {
        return (
            <Container>
                <div className="mt-5" style={myStyle.examBody}>
                    <p className="m-0">Congratulations! You have finished the exam.</p>
                </div>
            </Container>
        
        )
    }

    const renderStart = () => {
        return (
            <Container>
                <div className="text-center mt-5">
                    <h3>Are you ready ?</h3>
                    <Button onClick={() => setIsStart(!isStart)}>Start!</Button>
                </div>
            </Container>
       
        )
    }
            
    return isLoading ? <Loader /> : isStart ? result ? renderResult() : renderQuiz() : renderStart();  
}

const myStyle = {
    examBody: {
        border : "1px solid #ccc",
        padding  : "20px",
        borderRadius: "5px"
    }
   
}
export default Exam;
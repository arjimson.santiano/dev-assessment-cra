import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { Container } from 'reactstrap';

import QuestionnaireItem from './questionnaireItem';
import Loader from '../components/Loader';

function ManageQuestionnaires(props) {
    const [data, setData] = useState({
        questionnaires: []
    });

    const [isLoading, setisLoading] = useState(true);

    useEffect(() => {
        let fetchQuestions = setTimeout(() => getQuestionnaires(), 250);
        return () => {
            clearTimeout(fetchQuestions);
        }
     
    },[]);

    // FETCH ALL questionnaire
    const getQuestionnaires = () => {
        axios.get("http://localhost:5000/api/questionnaires")
            .then(res => {
                setData({ ...data, questionnaires: res.data });
                setisLoading(false);
            });
    };

    // DELETE questionnaire
    const deleteHandler = async (id) => {
        axios.delete(`http://localhost:5000/api/questionnaires/${id}`)
        .then(res => {
            setData({
                ...data,
                questionnaires: [...data.questionnaires.filter(q => q._id !== id)]
            });
        });
    };

    const sendExam = (id) =>{
        console.log(id,'sent!');
    }
    
    const goToExam = (id) =>{
        console.log(id,'sent!');
    }

    // questionnaires LISTS
    const questionnairesItem = data.questionnaires.map(questionnairesItem => {
        return <QuestionnaireItem 
        key={questionnairesItem._id} 
        {...questionnairesItem} 
        deleteHandler={deleteHandler} 
        sendExam={sendExam}
        goToExam={goToExam}
        />
    });

    return (
        isLoading ? <Loader /> :
        <Container>
           
            <h3 className="mt-3">Manage Questionnaires</h3>
            <Link className="btn btn-dark mt-2 cstm-btn" to="/manage-questionnaires/add-questionnaire" >Add Questionnaire</Link>
            <div style={style.table} className="mt-2">
                {questionnairesItem}
            </div>
        </Container>

    )
}

const style = {
    table: {
        border: "1px solid #ced4da",
        width: "100%",
        height: "500px",
        borderRadius: ".25rem",
        padding: ".5rem .35rem",
        overflowY: "auto"
    }
}

export default ManageQuestionnaires;
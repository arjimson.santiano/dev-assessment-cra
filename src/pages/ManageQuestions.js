import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Container, Button, NavLink } from 'reactstrap';
import swal from 'sweetalert';
import Loader from '../components/Loader';

import { Link } from 'react-router-dom';
import QuestionItem from './questionItem';


function ManageQuestions(props) {

    const [data, setData] = useState({
        questions: []
    });

    const [isLoading, setisLoading] = useState(true);

    useEffect(() => {
        // getQuestions()
        let fetchQuestions = setTimeout(() => getQuestions(), 250);
        return () => {
            clearTimeout(fetchQuestions);
        }

    },[]);

    const getQuestions = () => {
        let fetch = axios
            .get("http://localhost:5000/api/questions")
            .then(res => {
                setData({ ...data, questions: res.data });
                setisLoading(false);
            })
            .catch(err => {
                console.log(err);
            });
    };

    const deleteHandler = async (id) => {
        // is exist to questionnaires
        let isExist = await isExistToQuestionnaires(id); 
        console.log(isExist);
        // console.log(exist.length);
        const deleteQuestion = async () => {
            const del = await axios.delete(`http://localhost:5000/api/questions/${id}`)
                .then(res => {
                    setData({
                        ...data,
                        questions: [...data.questions.filter(q => q._id !== id)]
                    });
                    return true;
                }
                ).catch(err => { return false });
        }
      
        if(isExist) {
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then(async (willDelete) => {
                if (willDelete) {
                    let del = await deleteQuestion();
                    if(del) {
                        swal("Poof! Your imaginary file has been deleted!", {
                            icon: "success",
                        });
                    } else {
                        swal("Good job!", "You clicked the button!", "error");
                    }
                    
                } else {
                    swal("Good job!", "You clicked the button!", "error");
                }
            });
        } else {
            deleteQuestion();
        }
    }

    const isExistToQuestionnaires = async (id) => {
        let isExist = await axios.get(`http://localhost:5000/api/questions/is-exist/${id}`);

        if(isExist.data.res.length > 0) {
            return true;
        }
        return false;
    }

    const questionsItem = data.questions.length > 0 ?  data.questions.map(q => {
        return <QuestionItem question={q} deleteHandler={deleteHandler} />}) : "No records yet!";

    return (
        isLoading ? <Loader /> :
        <Container>
            <h3 className="mt-3">Manage Questions</h3>
            <Link to="/manage-questions/add-question" className="btn btn-dark mt-2 cstm-btn">Add Question</Link>
            <div style={style.table} className="mt-2">
                {questionsItem}
            </div>
        </Container>
    )
}

const style = {
    table: {
        border: "1px solid #ced4da",
        width: "100%",
        height: "80vh",
        borderRadius: ".25rem",
        padding: ".5rem .35rem",
        overflowY: "auto"
    }
}

export default ManageQuestions;
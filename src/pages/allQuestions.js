import React from 'react';
import { Link } from 'react-router-dom';
import { Button } from 'reactstrap';

function AllQuestions(props) {
    const { question, answer, category, questionType, _id, onSelect, isSelected } = props;
    console.log(props, "props")

    let questionText = question;
    if (questionText.length > 150) questionText = questionText.substring(0,150);
    questionText = questionText + "...";

    return (
        <div className={isSelected && "isSelected"} style={style.questionItem} onClick={() => onSelect(_id)}>
            <p className="m-0"><strong>{category}</strong></p>
            <p className="m-0">{questionType}</p>
            <p className="m-0"><strong>Q:</strong> {questionText}</p>
            <p className="m-0"><strong>A:</strong> {answer}</p>
        </div>
    )
}

const style = {
    questionItem: {
        border: "1px solid #ced4da",
        width: "100%",
        padding: ".15rem .25rem .45rem .25rem",
        borderRadius: ".25rem",
        cursor: "pointer",
        marginBottom: ".25rem",
        fontSize: ".85rem",
        position: "relative"
    },
    buttonStyle: {
        padding: ".05rem .55rem",
        fontSize: ".85rem"
    },
    actionsStyle: {
        position: "abosolute",
        float: "right"
    }
}
export default AllQuestions;
import React from 'react';
import { Link } from 'react-router-dom';
import { Button } from 'reactstrap';

function QuestionItem(props) {
    const { question, answer, category, questionType, _id, timeAllotment } = props.question;
    return (
        <div style={style.questionItem}>
            <p className="m-0"><strong>{category}</strong> <span className="mr-2" style={{float: "right"}}>{timeAllotment} {timeAllotment > 1 ? "mins" : "min"}</span></p>
            <p className="m-0">{questionType}</p>
            <p className="m-0"><strong>Q:</strong> {question}</p>
            <p className="m-0"><strong>A:</strong> {answer}</p>
            <div className="mt-1">
                <Link className="btn btn-dark mr-2" style={style.buttonStyle} to={`/manage-questions/question/${_id}`}>Edit</Link>
                <Button className="btn btn-dark" style={style.buttonStyle} onClick={() => props.deleteHandler(_id)}>Delete</Button>
            </div>
            
        </div>
    )
}

const style = {
    questionItem: {
        border: "1px solid #ced4da",
        width: "100%",
        padding: ".15rem .25rem .45rem .25rem",
        borderRadius: ".25rem",
        cursor: "pointer",
        marginBottom: ".25rem",
        fontSize: ".85rem",
        position: "relative"
    },
    buttonStyle: {
        padding: ".05rem .55rem",
        fontSize: ".85rem"
    },
    actionsStyle: {
        position: "abosolute",
        float: "right"
    }
}
export default QuestionItem;
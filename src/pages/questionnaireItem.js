import React from 'react';
import { Link } from 'react-router-dom';
import { Button } from 'reactstrap';

function QuestionItem(props) {
    const { title, _id, description, questions } = props;

 
    return (
        <div style={style.questionnaireItem}>
            <p className="m-0"><strong>{title}</strong></p>
            <p className="m-0">{description}</p>
            <p className="m-0"><strong>{questions.length}</strong> question{questions.length > 1 ? "s" : ""}</p>
            <div className="mt-1">
            <Link className="btn btn-dark mr-2" style={style.buttonStyle} to={`/manage-questionnaires/questionnaire/${_id}`}>Edit</Link>
            <Button className="btn btn-dark mr-2" style={style.buttonStyle} onClick={() => props.deleteHandler(_id)}>Delete</Button>
            <Button className="btn btn-dark mr-2" style={style.buttonStyle} onClick={() => props.sendExam(_id)}>Send</Button>
            <Link className="btn btn-dark" style={style.buttonStyle} to={`/exam/${_id}`} target="_blank">Go to Exam</Link>

            </div>
            
        </div>
        
  
    )
}

const style = {
    questionnaireItem: {
        border: "1px solid #ced4da",
        width: "100%",
        padding: ".15rem .25rem .45rem .25rem",
        borderRadius: ".25rem",
        cursor: "pointer",
        marginBottom: ".25rem",
        fontSize: ".85rem",
        position: "relative"
    },
    buttonStyle: {
        padding: ".05rem .55rem",
        fontSize: ".85rem"
    },
    actionsStyle: {
        position: "abosolute",
        float: "right"
    }
}
export default QuestionItem;
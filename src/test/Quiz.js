import React from 'react';



function Quiz(props) {
    const { question } = props;
    return (
        <h3>{question}</h3>
    )
}

export default Quiz;